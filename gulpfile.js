let gulp = require('gulp')
let uglify = require('gulp-uglify-es').default

gulp.task('minify', () => {
    return gulp
        .src('dist/**/*')
        .pipe(uglify())
        .pipe(gulp.dest('dist/'))
})
