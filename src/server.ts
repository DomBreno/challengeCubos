import 'module-alias/register'
import config from '@App/config'
import App from '@App/app'

const app = new App()

app.boot().listen(config.port)
