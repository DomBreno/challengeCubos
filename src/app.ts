import express, { Request, Response, NextFunction } from 'express'
import queryParser from 'express-query-parser'

import {
    bodyValidator,
    errorOnRequestBody,
    IFdateValidator,
    fieldsValidator,
} from './middlewares'

import routes from '@App/routes'
import config from '@App/config'

class App {
    public express: express.Application

    public constructor() {
        this.express = express()
        this.middlewares()
        this.routes()
    }

    public boot(): express.Application {
        console.log(`Setup:

        PORT: ${config.port}
        Filename: ${config.FILE}
        Host: http://localhost:${config.port}

        `)
        return this.express
    }

    private middlewares(): void {
        this.express.use(express.json())
        this.express.use(
            queryParser({
                parseNull: true,
                parseBoolean: true,
            }),
        )
        this.express.use(
            '/api/cadastrar-regra',

            (error: Error, req: Request, res: Response, next: NextFunction) =>
                errorOnRequestBody(error, req, res, next),

            (req: Request, res: Response, next: NextFunction) =>
                bodyValidator(req, res, next),
        )
        this.express.use(
            '/api/listar-regras',

            (req: Request, res: Response, next: NextFunction) =>
                IFdateValidator(req, res, next),

            (req: Request, res: Response, next: NextFunction) =>
                fieldsValidator(req, res, next),
        )
    }

    private routes(): void {
        this.express.use('/api', routes)
    }
}

export default App
