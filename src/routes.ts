import { Router } from 'express'

import {
    CreateMethods,
    ListMethods,
    DeleteMethods,
} from '@modules/Rules/Rules.controller'

const routes: Router = Router()

const _CreateMethods = new CreateMethods()
const _ListMethods = new ListMethods()
const _DeleteMethods = new DeleteMethods()

routes.post('/cadastrar-regra', (req, res) =>
    _CreateMethods.createRule(req, res),
)

routes.get(
    '/listar-regras',
    (req, res, next) => _ListMethods.listAllRules(req, res, next),
    (req, res, next) => _ListMethods.listByQuery(req, res),
)

routes.get('/regra/:id', (req, res, next) =>
    _ListMethods.listById(req, res, next),
)

routes.delete('/regra/:id', (req, res, next) =>
    _DeleteMethods.deleteById(req, res, next),
)

export default routes
