import { ObjectFile, Fields } from '@App/interfaces'
import CoreMethods from '@App/global/methods'
import { isArray, isString } from 'util'

interface DaysQuery {
    day: Date | string
    intervals: object[]
    nday?: number[]
}
export default class AuxiliarFunctions {
    idate: Date

    fdate: Date

    daysQuery: Array<DaysQuery>

    constructor(idate: Date, fdate: Date, daysQuery: Array<DaysQuery>) {
        this.idate = new Date(idate)
        this.fdate = new Date(fdate)
        this.daysQuery = daysQuery
    }

    public ruleConstructor(
        day: Array<string> | string,
        intervals: Array<object>,
    ): ObjectFile {
        return {
            id: Math.random()
                .toString(36)
                .substr(2, 9),
            day,
            intervals,
            nday: AuxiliarFunctions.getNday(day),
        }
    }

    public dateFormatter(dateString: string): string {
        return dateString
            .split('-')
            .reverse()
            .join('/')
    }

    private mainResolver(date: ObjectFile): void {
        date.nday.forEach(ndayNumber => {
            for (const ruleQuery of this.daysQuery) {
                if (ndayNumber === ruleQuery.nday[0]) {
                    if (date.day.indexOf('-') === -1) {
                        date.intervals.map((interval: object) =>
                            ruleQuery.intervals.push(interval),
                        )
                    } else {
                        if (isString(date.day)) {
                            const _d = new Date(this.dateFormatter(date.day))

                            if (_d >= this.idate && _d <= this.fdate) {
                                date.intervals.map(interval =>
                                    ruleQuery.intervals.push(interval),
                                )
                            }
                        }
                    }
                    break
                }
            }
        })
    }

    async filterDates(): Promise<Array<DaysQuery>> {
        ;(await CoreMethods.find({ all: true })).forEach(date =>
            this.mainResolver(date),
        )

        const finallyData = this.daysQuery.map(rule => {
            return {
                day: this.StandardDate(rule.day),
                intervals: rule.intervals,
            }
        })

        return finallyData
    }

    static getNday = (day: string | Array<string>): Array<number> => {
        if (isArray(day)) {
            const newNDay = []
            day.forEach(el => {
                el === 'Domingo'
                    ? newNDay.push(0)
                    : el === 'Segunda'
                    ? newNDay.push(1)
                    : el === 'Terça'
                    ? newNDay.push(2)
                    : el === 'Quarta'
                    ? newNDay.push(3)
                    : el === 'Quinta'
                    ? newNDay.push(4)
                    : el === 'Sexta'
                    ? newNDay.push(5)
                    : el === 'Sábado'
                    ? newNDay.push(6)
                    : ''
            })
            return newNDay
        } else if (day === 'Diariamente') {
            const newNDay = [0, 1, 2, 3, 4, 5, 6]
            return newNDay
        } else {
            const newNDay = new Date(
                AuxiliarFunctions.prototype.dateFormatter(day),
            ).getDay()
            return [newNDay]
        }
    }

    fieldsResolver(
        fields: Fields = { all: true },
        rule: ObjectFile,
    ): ObjectFile {
        if (fields.all) return rule

        fields.id ? 0 : delete rule.id
        fields.nday ? 0 : delete rule.nday
        fields.day ? 0 : delete rule.day
        fields.intervals ? 0 : delete rule.intervals

        return rule
    }

    StandardDate = (element: string | Date): string => {
        if (isString(element)) {
            element = new Date(
                AuxiliarFunctions.prototype.dateFormatter(element),
            )

            const day =
                element.getDate() < 10
                    ? `0${element.getDate()}`
                    : element.getDate()

            let month: number | string = element.getMonth() + 1
            month = month < 10 ? `0${month}` : month

            element = `${day}-${month}-${element.getFullYear()}`
            return element
        }
        const day =
            element.getDate() < 10 ? `0${element.getDate()}` : element.getDate()

        let month: number | string = element.getMonth() + 1
        month = month < 10 ? `0${month}` : month

        element = `${day}-${month}-${element.getFullYear()}`
        return element
    }
}
