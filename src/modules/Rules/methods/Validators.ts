import BaseJoi from '@hapi/joi'
import Extension from '@hapi/joi-date'
import { Fields } from '@App/global/methods'

const Joi = BaseJoi.extend(Extension)

interface Query {
    idate?: string
    fdate?: string
}

interface ValidatorReturn {
    message?: string
    status: boolean
}

const bodySchema = Joi.object().keys({
    day: Joi.alternatives()
        .try(
            Joi.array().items(Joi.string()),
            Joi.date()
                .format('DD-MM-YYYY')
                .raw(),
            Joi.string().equal('Diariamente'),
        )
        .required(),
    intervals: Joi.array().items(
        Joi.object({
            start: Joi.date()
                .format('HH:mm')
                .raw()
                .required(),
            end: Joi.date()
                .format('HH:mm')
                .raw()
                .required(),
        }),
    ),
})

const IFdateSchema = Joi.object().keys({
    idate: Joi.date()
        .format('DD-MM-YYYY')
        .raw()
        .required(),
    fdate: Joi.date()
        .format('DD-MM-YYYY')
        .raw()
        .required(),
})

const fieldsSchema = Joi.object().keys({
    all: Joi.boolean(),
    id: Joi.boolean(),
    day: Joi.boolean(),
    intervals: Joi.boolean(),
    nday: Joi.boolean(),
})

class Validator {
    public bodyValidator(rule: object): ValidatorReturn {
        const result = Joi.validate(rule, bodySchema)
        if (result.error === null) return { status: true }
        else {
            const message = result.error.details.map(
                (error: { message: string }) => {
                    return { message: error.message }
                },
            )
            return { message, status: false }
        }
    }

    public IFdateValidator(query: Query): ValidatorReturn {
        if (!query.idate && !query.fdate) return { status: true }

        const result = Joi.validate(query, IFdateSchema)

        if (result.error === null) return { status: true }
        else {
            const message = result.error.details.map(
                (error: { message: string }) => {
                    return { message: error.message }
                },
            )

            return { message, status: false }
        }
    }

    public fieldsValidator(fields: Fields): boolean {
        if (!fields) return true
        else {
            const result = Joi.validate(fields, fieldsSchema)
            if (result.error === null) return true
            else return false
        }
    }
}

export default new Validator()
