import { Request, Response, NextFunction } from 'express'
import { eachDayOfInterval } from 'date-fns'

import CoreMethods from '@App/global/methods'
import AuxFunctions from './methods/common'
import { Fields, ObjectFile } from '@App/interfaces'

class CreateMethods {
    ruleConstructor: Function
    constructor() {
        this.ruleConstructor = AuxFunctions.prototype.ruleConstructor
    }
    public createRule(req: Request, res: Response): Response {
        try {
            const newRule = this.ruleConstructor(
                req.body.day,
                req.body.intervals,
            )

            CoreMethods.create(newRule)
            return res.status(201).send('Regra salva com sucesso!')
        } catch (e) {
            return res.status(500).send('Falha ao salvar a regra!')
        }
    }
}

class ListMethods {
    dateFormatter: Function
    fieldsResolver: Function

    constructor() {
        this.dateFormatter = AuxFunctions.prototype.dateFormatter
        this.fieldsResolver = AuxFunctions.prototype.fieldsResolver
    }
    public async listAllRules(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        if (req.query.idate && req.query.fdate) return next()
        else
            return res.status(200).json(await CoreMethods.find(req.query.field))
    }

    public async listById(req: Request, res: Response): Promise<Response> {
        const File = await CoreMethods.getFile()
        const ruleByID = File.find(rule => rule.id === req.params.id)

        this.fieldsResolver(req.query.field, ruleByID)

        return res.status(200).json(ruleByID)
    }

    public async listByQuery(req: Request, res: Response): Promise<Response> {
        const idate = new Date(this.dateFormatter(req.query.idate))
        const fdate = new Date(this.dateFormatter(req.query.fdate))

        const daysQueries = eachDayOfInterval({
            start: idate,
            end: fdate,
        }).map(date => ({
            day: date,
            intervals: [],
            nday: [date.getDay()],
        }))

        const rulesByQueries = await new AuxFunctions(
            idate,
            fdate,
            daysQueries,
        ).filterDates()

        return res.status(200).json(rulesByQueries)
    }
}

class DeleteMethods {
    public async deleteById(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<Response | void> {
        if (!(await CoreMethods.deleteById(req.params.id)))
            return res.status(404).send('Regra não encontrada!')
        else res.status(200).send('Regra deletada!')
        return next()
    }
}

export { CreateMethods, ListMethods, DeleteMethods }
