export default interface ObjectFile {
    id?: string
    nday?: number[]
    day?: Array<string> | string
    intervals?: Array<object>
}
