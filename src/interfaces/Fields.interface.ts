export default interface Fields {
    all?: boolean
    id?: boolean
    day?: boolean
    nday?: boolean
    intervals?: boolean
}
