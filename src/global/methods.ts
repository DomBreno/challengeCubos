import fs from 'fs'

import AuxFunctions from '@modules/Rules/methods/common'
import config from '@App/config'
import { Fields, ObjectFile } from '../interfaces'

class CoreMethods {
    async create(rule: ObjectFile): Promise<void> {
        const File = await CoreMethods.prototype.getFile()
        File.push(rule)
        fs.writeFileSync(config.FILE, JSON.stringify(File))
    }

    public async getFile(): Promise<Array<ObjectFile>> {
        let dataFile: string

        if (fs.existsSync(config.FILE)) {
            dataFile = fs.readFileSync(config.FILE).toString()
            if ('' === dataFile || '[]' === dataFile) {
                fs.writeFileSync(config.FILE, '[]')
                return JSON.parse(dataFile)
            } else return JSON.parse(dataFile)
        } else {
            fs.writeFileSync(config.FILE, '[]')
            dataFile = fs.readFileSync(config.FILE).toString()
            return JSON.parse(dataFile)
        }
    }

    public async find(fields: Fields): Promise<Array<ObjectFile>> {
        const fieldsResolver = AuxFunctions.prototype.fieldsResolver
        const File = await CoreMethods.prototype.getFile()

        const FileByFields = File.map(rule => fieldsResolver(fields, rule))

        return FileByFields
    }

    public async deleteById(id: string): Promise<void | boolean> {
        const dataFile = await CoreMethods.prototype.getFile()

        const verify = dataFile.find(rule => rule.id === id)

        if (verify) {
            const finallyData = dataFile.filter(rule => !(rule.id === id))
            fs.writeFileSync(config.FILE, JSON.stringify(finallyData))

            return true
        } else return false
    }
}

export default new CoreMethods()
