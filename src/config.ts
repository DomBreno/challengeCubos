import { config } from 'dotenv'
config()

export default {
    port: process.env.PORT || 9090,
    FILE: process.env.FILE + '.json' || 'dataRules.json',
}
