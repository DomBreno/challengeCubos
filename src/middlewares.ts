import { Request, Response, NextFunction } from 'express'

import Validators from '@modules/Rules/methods/Validators'

export const bodyValidator = (
    req: Request,
    res: Response,
    next: NextFunction,
): Response | void => {
    const info = Validators.bodyValidator({
        day: req.body.day,
        intervals: req.body.intervals,
    })
    if (info.status) return next()
    else
        return res.status(406).send(
            `Body: ${JSON.stringify({
                day: req.body.day,
                intervals: req.body.intervals,
            })} inválido`,
        )
}

export const errorOnRequestBody = (
    error: Error,
    req: Request,
    res: Response,
    next: NextFunction,
): Response =>
    res.status(400).json({
        message: 'Not Acceptable/Bad Request',
        error: error.message,
    })

export const IFdateValidator = (
    req: Request,
    res: Response,
    next: NextFunction,
): Response | void => {
    const info = Validators.IFdateValidator({
        idate: req.query.idate,
        fdate: req.query.fdate,
    })

    if (info.status) return next()
    else return res.status(406).json(info.message)
}

export const fieldsValidator = (
    req: Request,
    res: Response,
    next: NextFunction,
): Response | void => {
    if (Validators.fieldsValidator(req.query.field)) return next()
    else {
        return res
            .status(400)
            .send(`Query: ${JSON.stringify(req.query.field)} inválida`)
    }
}
